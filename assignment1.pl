% Ques 1 ->
add(X) :-
    assert(X).

remove_db1(X) :-
    retract(db1(X)).

remove_db1_all :-
    retractall(db1(_)).

% Ques 2 ->
remove_db1_with_element_a :-
    retract(db1(a)).

% Ques 3 ->
remove_db2_with_element_func_a :-
    retract(db2(func(a))).

% Ques 4 -> Difference between retract(_) and retractall(_).
/* Ans: retract(_) function will only remove the only one occurance of fact at a time
        where retractall(_) will remove all the occurance of a fact at a time.
*/

% Ques 5 -> Difference between dif(X, Y) vs dif(X, _) vs dif(a, b) vs dif(X, a).
/* Ans: When dif(a, b) is applied to constants like dif(a, b) then it will works same as '\=' operator,
        but when any one of the arguments in dif is variable then the behaviour will change and output same input text.
*/

% Ques 6 ->
% Maximum between four Numbers.
max(W, X, Y, Z) :-
    W >= X 
        -> ( W >= Y 
            -> ( W >= Z   -> write(W)
                        ; write(Z) ) 
            ; ( Y >= Z    -> write(Y)
                        ; write(Z) ) )
        ; ( X >= Y 
            -> ( X >= Z -> write(X)
                        ; write(Z) ) 
            ; ( Y >= Z    -> write(Y)
                        ; write(Z) ) ).


% Maximum between four Numbers without using then '->' operator.
max2(W, X, Y, Z) :-
    W >= X,
        ( W >= Y,
            ( W >= Z, write(W) ; write(Z) ) ;  
            ( Y >= Z, write(Y) ; write(Z) ) 
        ) ; 
        ( X >= Y,
            ( X >= Z, write(X) ; write(Z) ) ;
            ( Y >= Z, write(Y) ; write(Z) )
        ).